/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.auth;

import com.patpat.jwt.GenerateToken;
import com.patpat.model.Admin;
import com.patpat.model.Agent;
import com.patpat.model.Customer;
import com.patpat.model.User;
import com.patpat.model.ErrorMessage;
import com.patpat.model.UserType;
import com.patpat.resource.CustomerResource;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

/**
 *
 * @author patpat
 */
public class Auth {

    private final String CUSTOMER_CLAIMS = "/customerClaims.json";
    private final String AGENT_CLAIMS = "/agentClaims.json";
    private final String ADMIN_CLAIMS = "/adminClaims.json";
    private String username;
    private String password;
    private Response response;

    public Auth(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Response login() {
        Optional<User> user = Optional.ofNullable(User.getByEmail(username));
        if (user.isPresent()) {
            User userToVerify = user.get();
            if (userToVerify.getPassword().equals(password)) {
                UserType userType = userToVerify.role;
                System.out.println("User Type: "+userType);
                switch (userType) {
                    case CUSTOMER:
                        response = customerLogin(userToVerify.id, userToVerify.password);
                        break;
                    case ADMIN:
                        response = adminLogin(userToVerify.id, userToVerify.password);
                        break;
                    case AGENT:
                        response = agentLogin(userToVerify.id, userToVerify.password);
                        break;
                }
            } else {
                ErrorMessage errorMessage = new ErrorMessage(
                        "Wrong credentials", 404, "CustomerNotFound"
                );
                response = Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
            }
        } else {
            ErrorMessage errorMessage = new ErrorMessage(
                    "No customer with such id", 404, "CustomerNotFound"
            );
            response = Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }
        
        return response;
    }

    private Response customerLogin(Long id, String password) {
        Customer customer = Customer.getById(id);
        try {
            customer.token = GenerateToken.generateToken(CUSTOMER_CLAIMS);
            return Response.ok(customer).build();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(500).build();
        }
    }
    

    private Response adminLogin(Long id, String password) {
        Admin admin = Admin.getById(id);
        try {
            admin.token = GenerateToken.generateToken(CUSTOMER_CLAIMS);
            return Response.ok(admin).build();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(500).build();
        }
    }

    private Response agentLogin(Long id, String password) {
        Agent agent = Agent.getById(id);
        try {
            agent.token = GenerateToken.generateToken(CUSTOMER_CLAIMS);
            return Response.ok(agent).build();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(500).build();
        }
    }

    
}
