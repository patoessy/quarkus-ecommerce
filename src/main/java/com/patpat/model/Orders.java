/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.model;

import com.patpat.model.OrderItem;
import com.patpat.model.Customer;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author patpat
 */
@Entity
@Table(name = "order_table")
public class Orders extends PanacheEntity {

    public int amount;
    public Date date;
    @Column(name = "order_status")
    public String status;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "customer_id")
    public Customer customer;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "order_details")
    public List<OrderItem> orderItems;

    public static List<Orders> getByCustomer(Customer customer) {
        List<Orders> orders = Orders.find("customer", customer).list();
        return orders;
    }
}
