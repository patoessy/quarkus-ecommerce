/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.model;

import com.patpat.model.Cart;
import com.patpat.model.Customer;
import com.patpat.model.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.runtime.JpaOperations;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.ws.rs.core.Response;

/**
 *
 * @author patpat
 */
@Entity
@Table(name="cart")
public class Cart extends PanacheEntity{

    public static Cart getCartById(long cartId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    public int qty;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    public Product product;
    @JoinColumn(name = "customer_id")
    @OneToOne(fetch = FetchType.LAZY)
    public Customer customer;
    
    @JsonIgnore
    @Override
    public boolean isPersistent(){
        return JpaOperations.isPersistent(this);
    }
    
    public static List<Cart> getCartByCustomer(Customer customer){
        return Cart.find("customer", customer).list();
    }
}
