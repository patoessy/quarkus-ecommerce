/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.model;

/**
 *
 * @author patpat
 */
public class ErrorMessage {
    public String message;
    public int statusCode;
    public String error;

    public ErrorMessage(String message, int statusCode, String error) {
        this.message = message;
        this.statusCode = statusCode;
        this.error = error;
    }
}
