/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.model;

import com.patpat.model.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.runtime.JpaOperations;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author patpat
 */
@Entity
@Table(name = "customer")
public class Customer extends User{
    public String address;

    public static List<Customer> getAllCustomers() {
        return Customer.listAll(Sort.by("date", Sort.Direction.Descending));
    }

    public static Customer getById(long customerId) {
        return find("id", customerId).firstResult();
    }

    public static Customer getByEmail(String email) {
        return find("email", email).firstResult();
    }

    public static Customer getByPhone(String phone) {
        return find("phone", phone).firstResult();
    }

    public static List<Customer> getAllCustomers(int page, int resultNumber) {
        PanacheQuery<Customer> allCustomers = Customer.findAll();
        return allCustomers.page(Page.of(page, resultNumber)).list();
    }

    public static int totalPages() {
        PanacheQuery<Customer> allCustomers = Customer.findAll();
        return allCustomers.pageCount();
    }
}