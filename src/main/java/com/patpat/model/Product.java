/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.model;

import com.patpat.model.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.runtime.JpaOperations;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author patpat
 */
@Entity
@Table(name = "product")
public class Product extends PanacheEntity{
    public String name;
    public String description;
    public float price;
    public String category;
    public String quantity;
    //TODO: v0.3 add image support
    
    @JsonIgnore
    public boolean isPersistent(){
        return JpaOperations.isPersistent(this);
    }
    
    public static List<Product> getProducts(){
        //TODO: v0.3 Pagination
        List<Product> products = Product.listAll();
        return products;
    }
    
    public static Product getProductById(long id){
        return Product.find("id", id).firstResult();
    }
    
    public static List<Product> searchByName(int name){
        //TODO: v0.3 implement hibernate search
        return Product.findAll().list();
    }
}
