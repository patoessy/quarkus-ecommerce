/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.runtime.JpaOperations;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author patpat
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "user")
public class User extends PanacheEntity {

    public String fullnames;
    public String password;
    @Column(name = "date")
    @JsonProperty("date")
    public Date dateJoined;
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    public UserType role;
    @Column(name = "phone_number")
    public Long phoneNumber;
    public String email;
    @Transient
    public String token;

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty
    public String getToken() {
        return token;
    }

    @JsonIgnore
    public boolean isPersistent() {
        return JpaOperations.isPersistent(this);
    }
    
    public static User getByEmail(String email) {
        return find("email", email).firstResult();
    }
}
