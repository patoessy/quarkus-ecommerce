/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author patpat
 */
@Entity
@Table(name = "agent")
public class Agent extends User{
    public String location;
    @Column(name = "business_name")
    public String businessName;
    public String city;
    @Column(name = "national_id")
    public int nationalID;
    public static Agent getById(long agentId) {
        return find("id", agentId).firstResult();
    }
}
