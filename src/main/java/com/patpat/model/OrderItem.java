/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.model;

import com.patpat.model.Product;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author patpat
 */
@Entity
@Table(name = "order_items")
public class OrderItem extends PanacheEntity {

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    public Product product;
    public int quantity;
}
