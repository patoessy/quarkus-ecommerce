/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.resource;

import com.patpat.model.ErrorMessage;
import com.patpat.model.Product;
import java.util.Optional;
import javax.annotation.security.RolesAllowed;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author patpat
 */
@Path("/api/products")
public class ProductService {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProducts() {
        //TODO: v1.3 Paginate this endpoint
        return Response.ok(Product.getProducts()).build();
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProduct(@PathParam("id") long id) {
        Optional<Product> product = Optional.ofNullable(Product.getProductById(id));
        if (product.isPresent()) {
            return Response.ok(product.get())
                    .build();
        } else {
            ErrorMessage errorMessage = new ErrorMessage(
                    "No such product found", 404, "ProductNotFound"
            );
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(errorMessage)
                    .build();
        }
    }
    
    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //@RolesAllowed("ADMIN")
    public Response createProduct(Product p){
        p.id = null;
        Product product = p;
        Product.persist(product);
        return Response.status(Response.Status.CREATED)
                .entity(product)
                .build();
    } 
}
