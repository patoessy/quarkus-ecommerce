package com.patpat.resource;

import com.patpat.jwt.GenerateToken;
import com.patpat.model.Customer;
import com.patpat.model.ErrorMessage;
import com.patpat.model.UserType;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("/api/users/customer")
public class CustomerResource {

    @GET
    @RolesAllowed("Customer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Customer> getAllCustomers() {
        return Customer.getAllCustomers();
    }

    @GET
    @Path("{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByEmail(@PathParam("email") String email) {
        Optional<Customer> customer = Optional.ofNullable(Customer.getByEmail(email));
        if (customer.isPresent()) {
            return Response.ok(customer.get()).build();
        } else {
            ErrorMessage errorMessage = new ErrorMessage(
                    "No customer with such email", 404, "CustomerNotFound"
            );
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }
    }

    @GET
    @Path("{phone}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByPhone(@PathParam("phone") String phone) {
        Optional<Customer> customer = Optional.ofNullable(Customer.getByPhone(phone));
        if (customer.isPresent()) {
            return Response.ok(customer.get()).build();
        } else {
            ErrorMessage errorMessage = new ErrorMessage(
                    "No customer with such mobile number", 404, "CustomerNotFound"
            );
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByEmail(@PathParam("id") int id) {
        Optional<Customer> customer = Optional.ofNullable(Customer.getById(id));
        if (customer.isPresent()) {
            return Response.ok(customer.get()).build();
        } else {
            ErrorMessage errorMessage = new ErrorMessage(
                    "No customer with such id", 404, "CustomerNotFound"
            );
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }
    }

    @GET
    @Path("/auth/{email}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response customerLogin(
            @PathParam("email") String email,
            @PathParam("password") String password
    ) {
        Optional<Customer> customer = Optional.ofNullable(Customer.getByEmail(email));
        if (customer.isPresent()) {
            Customer customerToVerify = customer.get();
            String customerClaims = "/customerClaims.json";
            try {
                customerToVerify.token = GenerateToken.generateToken(customerClaims);
            } catch (Exception ex) {
                Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (customerToVerify.getPassword().equals(password)) {
                return Response.ok()
                        .entity(customerToVerify)
                        .build();
            } else {
                ErrorMessage errorMessage = new ErrorMessage(
                        "Wrong credentials", 404, "CustomerNotFound"
                );
                return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
            }
        } else {
            ErrorMessage errorMessage = new ErrorMessage(
                    "No customer with such id", 404, "CustomerNotFound"
            );
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @PermitAll
    public Response createCustomer(Customer customer) {
        System.out.println(customer.password);
        customer.id = null;
        Customer customerNew = customer;
        customerNew.role = UserType.CUSTOMER;
        Customer.persist(customerNew);
        return Response.status(Response.Status.CREATED)
                .entity(customer)
                .build();
    }
}
