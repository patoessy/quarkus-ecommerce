/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.resource;

import com.patpat.model.Cart;
import com.patpat.model.Customer;
import com.patpat.model.ErrorMessage;
import com.patpat.model.Product;
import com.patpat.pojo.CartPojo;
import java.util.List;
import java.util.Optional;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author patpat
 */
@RequestScoped
@Path("/api/cart")
public class CartResource {

    @GET
    @Path("/customer/{user_id}")
    @RolesAllowed("CUSTOMER")
    @Produces(MediaType.APPLICATION_JSON)
    public Response customerCart(@PathParam("user_id") long userId) {
        Optional<Customer> customer = Optional.ofNullable(Customer.getById(userId));
        Optional<List<Cart>> cart = null;
        if (customer.isPresent()) {
            cart = Optional.ofNullable(Cart.getCartByCustomer(customer.get()));
        }
        if (cart.isPresent()) {
            return Response.ok(cart.get())
                    .build();
        }
        ErrorMessage errorMessage = new ErrorMessage("Cart for such customer does not exist", 404, "NOT_FOUND");
        return Response.status(Response.Status.NOT_FOUND)
                .entity(errorMessage)
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("CUSTOMER")
    @Transactional
    public Response createCart(CartPojo cartPojo) {
        Cart cart = new Cart();
        Optional<Customer> customer = Optional.ofNullable(Customer.getById(cartPojo.customerId));
        if (customer.isPresent()) {
            cart.customer = customer.get();
        }
        Optional<Product> product = Optional.ofNullable(Product.getProductById(cartPojo.productId));
        if (product.isPresent()) {
            cart.product = product.get();
        }
        if (product.isEmpty() || customer.isEmpty()) {
            ErrorMessage em = new ErrorMessage(
                    "Customer or product cannot be null", 500, "ServerError");
            return Response.status(500)
                    .entity(em)
                    .build();
        }
        cart.qty = cartPojo.qty;
        Cart.persist(cart);
        return Response.status(Response.Status.CREATED)
                .entity(cart)
                .build();
    }
}
