/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.resource;

import com.patpat.model.Cart;
import com.patpat.model.Customer;
import com.patpat.model.OrderItem;
import com.patpat.model.Orders;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.annotation.security.RolesAllowed;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author patpat
 */
@Path("/api/order")
public class OrderResource {

    @POST
    @Path("/{customer_id}")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("CUSTOMER")
    @Transactional
    public Response checkout(
            @PathParam("customer_id") long customerId
    ) {
        Optional<Customer> customerOptinal = Optional.ofNullable(Customer.getById(customerId));
        Orders order = new Orders();
        if (customerOptinal.isPresent()) {
            Customer customer = customerOptinal.get();
            Optional<List<Cart>> cart = Optional.ofNullable(Cart.getCartByCustomer(customer));
            if (cart.isPresent()) {
                order.customer = customer;
                order.date = new Date();
                int amount = 0;
                List<Cart> cartList = cart.get();
                List<OrderItem> items = new ArrayList();
                for (Cart cartHere : cartList) {
                    OrderItem orderItem = new OrderItem();
                    amount += cartHere.product.price;                    
                    orderItem.product = cartHere.product;
                    orderItem.quantity = cartHere.qty;
                    System.out.println(orderItem.product.price + "," + orderItem.quantity);
                    items.add(orderItem);
                    cartHere.delete();
                }
                OrderItem.persist(items);
                order.orderItems = items;
                order.amount = amount;
                order.status = "PENDING";
                order.persist();

                //order.orderItems.add();
                //OrderEntity.persist(order);
            }
        }
        return Response.status(201).entity(order).build();
    }
    
    @GET
    @Path("{customer_id}")
    @Produces(MediaType.APPLICATION_JSON)
    //@RolesAllowed("CUSTOMER")
    public Response getOrder(@PathParam("customer_id") long customerId){
        Optional<Customer> customerOptinal = Optional.ofNullable(Customer.getById(customerId));
        List<Orders> order;
        if (customerOptinal.isPresent()) {
            order = Orders.getByCustomer(customerOptinal.get());
            return Response.status(200).entity(order).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND)
                    .build();
        }
    }
}
