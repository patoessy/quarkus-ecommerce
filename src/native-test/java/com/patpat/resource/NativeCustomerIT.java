package com.patpat.resource;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeCustomerIT extends CustomerResourceTest {

    // Execute the same tests but in native mode.
}