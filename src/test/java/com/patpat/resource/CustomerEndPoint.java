/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.resource;

import com.patpat.pojos.Credentials;
import com.patpat.pojos.Customer;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;

/**
 *
 * @author patpat
 */
public class CustomerEndPoint {
    public Customer createCustomer(Customer c){
        Customer customer =  given().contentType(ContentType.JSON)
                .body(c)
                .when().post("/api/users/customer")
                .then()
                .statusCode(201)
                .extract().as(Customer.class);
        return customer;
    }

    public Customer login(String email, String password) {
        Credentials c = new Credentials(email, password);
        Customer customer = given().
                when().contentType(ContentType.JSON).body(c)
                .given().post("/api/auth")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .extract().as(Customer.class);
        return customer;
    }
}
