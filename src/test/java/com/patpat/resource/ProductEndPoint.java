/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.resource;

import com.patpat.pojos.Product;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

/**
 *
 * @author patpat
 */
public class ProductEndPoint {
    public Product createProduct(Product p){
        Product product =  RestAssured.given().contentType(ContentType.JSON)
                .body(p)
                .when().post("/api/products")
                .then()
                .statusCode(201)
                .extract().as(Product.class);
        return product;
    }
}
