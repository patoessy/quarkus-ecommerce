/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.resource;

import com.patpat.pojos.Customer;
import com.patpat.pojos.Order;
import com.patpat.pojos.CartPojo;
import com.patpat.pojos.Cart;
import com.patpat.pojos.Product;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author patpat
 */
@QuarkusTest
public class CartResourceTest {
    
    CartResourceEndPoint cartResource;
    CustomerEndPoint customerEndPoint;
    ProductEndPoint productEndPoint;
    OrderEndpoint orderEndpoint;
    Customer customer;
    Product product;
    Customer loggedInCustomer;
    String token;
    
    @BeforeEach
    void setup() {
        cartResource = new CartResourceEndPoint();
        customerEndPoint = new CustomerEndPoint();
        productEndPoint = new ProductEndPoint();
        orderEndpoint = new OrderEndpoint();
        customer = customerEndPoint.createCustomer(Customer.create());
        product = productEndPoint.createProduct(Product.create());
        loggedInCustomer = customerEndPoint.login(customer.email, customer.password);
        token = loggedInCustomer.token;
    }

    /**
     * Test of createCart method, of class CartResource.
     */
    @Test
    public void testCartEndpoint() {
        //create cart
        //verify user not empty
        //verify product not empty
        //retrive cart
        //Place order
        Cart cart = new Cart();
        cart.customerId = customer.id;
        cart.productId = product.id;
        cart.qty = 3;
        CartPojo cartPojo = cartResource.createCart(token, cart);
        List<CartPojo> retrivedCart = cartResource.getCart(token, customer.id);        
        Order order = orderEndpoint.placeOrder(token, customer.id);
        
        assertThat("Order is in pending state", order.Status.equals("PENDING"));
        assertEquals(1, retrivedCart.size());
        assertEquals(cartPojo.qty, cart.qty);
        //assertEquals(cart.qty, retrivedCart.qty);
    }
}
