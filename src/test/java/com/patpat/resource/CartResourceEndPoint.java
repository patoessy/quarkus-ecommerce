/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.resource;

import com.patpat.pojos.Cart;
import com.patpat.pojos.CartPojo;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import java.util.Date;
import java.util.List;

/**
 *
 * @author patpat
 */
public class CartResourceEndPoint {

    public CartResourceEndPoint() {

    }

    public CartPojo createCart(String token, Cart cart) {
        CartPojo cartPojo = given()
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .body(cart)
                .when().post("/api/cart")
                .then()
                .statusCode(201)
                .contentType(ContentType.JSON)
                .extract().as(CartPojo.class);
        return cartPojo;
    }

    //TODO: add checkout,delete cart and update cart tests
    public List<CartPojo> getCart(String token, int id) {
        List<CartPojo> cartPojo = given()
                .auth().oauth2(token)
                .pathParam("user_id", id)
                .when().get("/api/cart/customer/{user_id}")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .extract().body().jsonPath().getList(".",CartPojo.class);
        return cartPojo;
    }
}
