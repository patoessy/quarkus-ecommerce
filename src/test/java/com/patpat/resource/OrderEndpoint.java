/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.resource;

import com.patpat.pojos.Order;
import static io.restassured.RestAssured.given;
import io.restassured.authentication.OAuthSignature;
import io.restassured.http.ContentType;
import java.util.List;

/**
 *
 * @author patpat
 */
public class OrderEndpoint {

    public Order placeOrder(String token, int customerId) {
        Order order = given()
                .auth().oauth2(token)
                .when().get("/api/order/{customerId}", customerId)
                .then()
                .statusCode(201)
                .contentType(ContentType.JSON)
                .extract().as(Order.class);
        return order;
    }

    public List<Order> retriveOrders(String token, int customerId) {
        List<Order> orders = given().auth().oauth2(token)
                .when().get("/api/order/{customerId}", customerId)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .extract().body().jsonPath().getList(".", Order.class);
        return orders;
    }

}
