/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.pojos;

import java.util.Date;

/**
 *
 * @author patpat
 */
public class Customer {

    public int id;
    public Date date;
    public String email;
    public String fullnames;
    public long phoneNumber;
    public String role;
    public String token;
    public String address;
    public String password;
    
    public static Customer create(){
        Customer customer = new Customer();
        customer.address="342-10000, Thika";
        customer.date = new Date();
        customer.email="patumusembi70@gmail.com";
        customer.fullnames = "Patrick Musembi";
        customer.password = "46464564";
        customer.role="CUSTOMER";
        customer.phoneNumber=0702545654;
        return customer;
    }
}
