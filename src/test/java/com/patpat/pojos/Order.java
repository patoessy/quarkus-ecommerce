/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpat.pojos;

import java.util.Date;
import java.util.List;

/**
 *
 * @author patpat
 */
public class Order {
    public int amount;
    public int id;
    public Date date;
    public String Status;
    public Customer customer;
    public List<OrderItem> orderItems;
}
